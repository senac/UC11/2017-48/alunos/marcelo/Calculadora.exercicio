package br.com.senac.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtresultado;
    private Button btnsete;
    private Button btnoito;
    private Button btnnove;
    private Button btnquatro;
    private Button btncinco;
    private Button btnseis;
    private Button btnmultiplicar;
    private Button btntres;
    private Button btndois;
    private Button btnum;
    private Button btnvirgula;
    private Button btnzero;
    private Button btnlimpar;
    private Button btnsomar;
    private Button btndividir;
    private Button btnsubtrair;
    private boolean limpaResultado = true;
    private boolean limpaDisplay = true;



    private void init(){

        this.txtresultado = findViewById(R.id.resultado);
        this.btnsete = findViewById(R.id.btnsete);
        this.btnoito = findViewById(R.id.btnoito);
        this.btnnove = findViewById(R.id.btnnove);
        this.btnquatro = findViewById(R.id.btnquatro);
        this.btncinco = findViewById(R.id.btncinco);
        this.btnseis = findViewById(R.id.btnseis);
        this.btnmultiplicar = findViewById(R.id.btnmultiplicar);
        this.btntres = findViewById(R.id.btntres);
        this.btndois = findViewById(R.id.btndois);
        this.btnum = findViewById(R.id.btnum);
        this.btnvirgula = findViewById(R.id.btnvirgula);
        this.btnzero = findViewById(R.id.btnzero);
        this.btnlimpar = findViewById(R.id.btnlimpar);
        this.btnsomar = findViewById(R.id.btnsomar);
        this.btndividir = findViewById(R.id.btndividir);
        this.btnsubtrair = findViewById(R.id.btnsubtrair);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txtresultado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();

                if (limpaResultado){
                    txtresultado.setText(textoDoBotao);
                    limpaDisplay = false;
                }else {
                    txtresultado.setText(txtresultado.getText()+ textoDoBotao);
                }

            }
        });



    }
}
